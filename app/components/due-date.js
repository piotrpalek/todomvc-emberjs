import Ember from 'ember';
import moment from 'moment';

const { isEmpty } = Ember;

export default Ember.Component.extend({
  repo: Ember.inject.service(),
  classNames: ['due-date'],

  showInput: false,

  actions: {
    showDueDate() {
      this.set('showInput', true);

      Ember.run.scheduleOnce('afterRender', () => {
        this.$('input').focus();
      });
    },

    doneAddingDueDate(value) {
      if (isEmpty(value)) {
        this.set('todo.dueDate', null);
        this.set('showInput', false);
        return;
      }

      let date = moment(value, 'YYYY-MM-DD', true);

      if (date.isValid()) {
        this.get('repo').persist();
      } else {
        this.set('todo.dueDate', this.get('_previousDueDate'));
        this.set('_previousDueDate', null);
        Ember.run.later(() => {
          this.set('showInput', false);
        }, 800);
      }
    }
  }
});
