import Ember from 'ember';

const { computed } = Ember;

export default Ember.Component.extend({
	repo: Ember.inject.service(),
	tagName: 'li',
	editing: false,
	classNameBindings: ['todo.completed', 'editing', 'isDue'],

  isDue: computed('todo.dueDate', 'todo.completed', {
    get() {
      if (this.get('todo.completed')) {
        return false;
      }

      let dueDate = moment(this.get('todo.dueDate'), 'YYYY-MM-DD', true);
      if (!dueDate.isValid()) {
        return false;
      }

      let today = moment().milliseconds(0).seconds(0).minutes(0).hours(0);
      return dueDate.isSameOrBefore(today);
    }
  }),

	actions: {
		startEditing() {
			this.get('onStartEdit')();
			this.set('editing', true);
			Ember.run.scheduleOnce('afterRender', this, 'focusInput');
		},

		doneEditing(todoTitle) {
			if (!this.get('editing')) { return; }
			if (Ember.isBlank(todoTitle)) {
				this.send('removeTodo');
			} else {
				this.set('todo.title', todoTitle.trim());
				this.set('editing', false);
				this.get('onEndEdit')();
			}
		},

		handleKeydown(e) {
			if (e.keyCode === 13) {
				e.target.blur();
			} else if (e.keyCode === 27) {
				this.set('editing', false);
			}
		},

		toggleCompleted(e) {
			let todo = this.get('todo');
			Ember.set(todo, 'completed', e.target.checked);
			this.get('repo').persist();
		},

		removeTodo() {
			this.get('repo').delete(this.get('todo'));
		}
	},

	focusInput() {
		this.element.querySelector('input.edit').focus();
	}
});
